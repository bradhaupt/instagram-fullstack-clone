<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Contracts\Pagination\CursorPaginator;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;


class PostController extends Controller
{
	/**
	 * Display a listing of all posts.
	 *
	 * @return CursorPaginator
	 */
	public function index(): CursorPaginator
	{
		return Post::orderBy('id', 'DESC')->cursorPaginate();
	}

	/**
	 * Display a listing of posts from followed users.
	 *
	 * @return CursorPaginator
	 */
	public function feed(): CursorPaginator
	{
		return Post::where(function($query){
			$userIds = auth()->user()->following()->pluck('user_id');
			$query->whereUserId(auth()->id())->orWhereIn('user_id', $userIds);
		})->orderBy('id', 'DESC')->cursorPaginate();
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Collection
	 */
	public function store(Request $request): Collection
	{
		$attributes = $request->validate([
			'description' => 'required',
			'image' => ['required', 'image']
		]);

		$attributes['user_id'] = auth()->id();
		$attributes['slug'] = Str::random(10);
		$attributes['image'] = '/storage/' . $request->file('image')->store('images');
		Post::create($attributes);

		return Post::where('slug', $attributes['slug'])->get();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param Post $post
	 * @return Post $post
	 */
	public function show(Post $post): Post
	{
		return $post;
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @param string $slug
	 * @return Collection
	 */
	public function update(Request $request, string $slug): Collection
	{
		$attributes = $request->validate([
			'description' => 'required',
		]);

		if ($request->file('image')) {
			$attributes['image'] = '/storage/' . $request->file('image')->store('images');
		}

		$hasUpdated = Post::where('slug', $slug)->update($attributes);
		if ($hasUpdated) {
			return Post::where('slug', $slug)->get();
		}
		abort(500, "Couldn't update post");
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Post $post
	 * @return Bool
	 */
	public function destroy(Post $post): bool
	{
		return $post->delete();
	}
}
