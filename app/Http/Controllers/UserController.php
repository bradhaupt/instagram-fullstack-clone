<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\Pagination\CursorPaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request): Response
	{
		$fields = $request->validate([
			'first_name' => 'required|string',
			'last_name' => 'required|string',
			'username' => 'required|string|unique:users,username',
			'email' => 'required|string|unique:users,email',
			'password' => 'required|string|confirmed',
			'image' => 'required|image'

		]);

		$fields['password'] =  bcrypt($fields['password']);
		$fields['image'] = '/storage/' . $request->file('image')->store('images');

		$user = User::create($fields);

		$token = $user->createToken('remember_token')->plainTextToken;

		$response = [
			'user' => $user,
			'token' => $token
		];

		return response($response, 201);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param string $username
	 * @return User
	 */
	public function show(string $username): User
	{
		return User::withCount('posts')->where('username', $username)->first();
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @return Builder[]|Collection|User[]
	 */
	public function update(Request $request)
	{
		$attributes = $request->validate([
			'first_name' => ['required','string'],
			'last_name' => ['required','string'],
			'username' => ['required','string',Rule::unique('users')->ignore(auth()->id())],
			'email' => ['required','string',Rule::unique('users')->ignore(auth()->id()),'email'],
		]);

		if ($request->file('image')) {
			$attributes['image'] = '/storage/' . $request->file('image')->store('images');
		}

		$hasUpdated = User::where('id', auth()->id())->update($attributes);
		if ($hasUpdated) {
			return User::withCount('posts')->where('id', auth()->id())->get();
		}
		abort(500, "Couldn't update user");
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param string $username
	 * @return string
	 */
	public function destroy(string $username): string
	{
		if (User::where('username', $username)->first()) {
			User::where('username', $username)->first()->delete();
			return $username . ' deleted';
		}
		return 'User does not exist';
	}

	/**
	 * Search by username.
	 *
	 * @param string $q
	 * @return string
	 */
	public function search(string $q): string
	{
		return User::where('username', 'like', '%' . $q . '%')
			->orWhere('first_name', 'like', '%' . $q . '%')
			->orWhere('last_name', 'like', '%' . $q . '%')
			->take(15)
			->get();
	}

	/**
	 * Return user's posts
	 *
	 * @param User $user
	 * @return CursorPaginator
	 */
	public function posts(User $user): CursorPaginator
	{
		return $user->posts()->orderBy('id', 'DESC')->cursorPaginate();
	}
}
