<?php

namespace App\Http\Controllers;

use App\Models\Follow;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class FollowController extends Controller
{
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Follow|Model
	 */
	public function store(Request $request)
	{
		$attributes = $request->validate([
			'user_id' => 'required',
		]);

		$attributes['follower_id'] = auth()->id();
		return Follow::create($attributes);
	}
	/**
	 * Delete a resource.
	 *
	 * @param int $id
	 * @return bool
	 */
	public function destroy(int $id){
		return Follow::where('user_id', $id)->where('follower_id', auth()->id())->delete();
	}
}
