<?php

namespace App\Http\Controllers;

use App\Models\Like;
use App\Models\Post;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class LikeController extends Controller
{
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Like|Model
	 */
	public function store(Request $request)
	{
		$attributes = $request->validate([
			'post_id' => 'required',
		]);

		$attributes['user_id'] = auth()->id();
		return Like::create($attributes);
	}
	/**
	 * Delete a resource.
	 *
	 * @param Like $like
	 * @return bool
	 */
	public function destroy(Like $like): bool
	{
		return $like->delete();
	}
}
