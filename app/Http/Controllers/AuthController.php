<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
	/**
	 * Return currently logged-in user
	 *
	 * @return User
	 */
	public function me(): User
	{
		return User::withCount('posts')->where('id', auth()->id())->first();
	}

	/**
	 * Handle an authentication attempt.
	 *
	 * @param Request $request
	 * @return User
	 */
	public function login(Request $request): User
	{
		$credentials = $request->validate([
			'email' => ['required', 'email'],
			'password' => ['required'],
		]);

		if (Auth::attempt($credentials)) {
			$request->session()->regenerate();

			return User::withCount('posts')->where('id', auth()->id())->first();
		}

		abort(400, 'The provided credentials do not match our records.');
	}

	/**
	 * Log the user out of the application.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function logout(Request $request): Response
	{

		$request->session()->invalidate();

		$request->session()->regenerateToken();

		return response("", 200);
	}
}
