<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CommentController extends Controller
{
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$attributes = $request->validate([
			'comment' => 'required',
			'post_id' => 'required'
		]);

		$attributes['user_id'] = auth()->id();

		$commentId = Comment::create($attributes)->id;
		return Comment::where('id', $commentId)->get();

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\Comment  $comment
	 * @return Bool
	 */
	public function destroy(Comment $comment)
	{
		return $comment->delete();
	}
}
