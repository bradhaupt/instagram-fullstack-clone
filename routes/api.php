<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\FollowController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CommentController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Login or signup
Route::post('/login', [AuthController::class, 'login']);
Route::apiResource('users', UserController::class, [
	'only' => ['store']
]);

//Protected Routes
Route::middleware('auth:sanctum')->group(function(){
	Route::apiResource('posts', PostController::class);
	Route::get('/feed', [PostController::class, 'feed']);

	Route::apiResource('users', UserController::class, [
		'except' => ['store']
	]);
	Route::get('/users/search/{q}', [UserController::class, 'search']);
	Route::get('/users/{user:username}/posts', [UserController::class, 'posts']);

	Route::apiResource('comments', CommentController::class);

	Route::post('/logout', [AuthController::class, 'logout']);
	Route::get('/me', [AuthController::class, 'me']);

	Route::post('likes', [LikeController::class, 'store']);
	Route::delete('likes/{like:id}', [LikeController::class, 'destroy']);

	Route::post('follow/', [FollowController::class, 'store']);
	Route::delete('follow/{id}', [FollowController::class, 'destroy']);
});
