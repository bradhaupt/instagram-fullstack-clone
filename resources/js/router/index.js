import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Discover from "../views/Discover.vue";
import Create from "../views/Create.vue";
import Signup from "../views/Signup.vue";
import User from "../views/User.vue";
import Login from "../views/Login.vue";
import store from '../store/index'
import cookies from "vue-cookies";

Vue.use(VueRouter);

const routes = [
	{
		path: "/",
		name: "Home",
		component: Home,
	},
	{
		path: "/discover",
		name: "Discover",
		component: Discover,
	},
	{
		path: "/about",
		name: "About",
	},
	{
		path: "/create-post",
		name: "Create",
		component: Create,
	},
	{
		path: "/signup",
		name: "Signup",
		component: Signup,
	},
	{
		path: "/user",
		name: "User",
		component: User,
		children: [
			{
				path: ':username',
				component: User,
			}
		]
	},
	{
		path: "/Login",
		name: "Login",
		component: Login,
	},
	{
		path: "/*",
		redirect: '/'
	},
];

const router = new VueRouter({
	routes,
	mode: "history"
});

router.beforeEach(async (to, from, next) => {

	// Cancel any active fetches
	const controller = store.getters['getController'];
	if (controller) {
		controller.abort();
	}

	// Clear any persisting modal background
	let modalBackground = document.querySelector('.modal-backdrop')
	if (modalBackground) {
		modalBackground.remove()
	}

	// If user is not authenticated, do a fetch to check if session cookie is still valid
	const isAuthenticated = store.getters['getToken'];

	if (!isAuthenticated && to.name !== 'Login' && to.name !== 'Signup') {
		try {
			const res = await fetch(`/api/me`, {
				headers: {
					Accept: 'application/json'
				}
			});

			if (!res.ok) {
				return next({name: 'Login'})
			}
			const json = await res.json();
			const xsrfToken = cookies.get('XSRF-TOKEN');
			await store.dispatch('setCurrentUser', {user: json, xsrfToken});
			next();
		} catch (err) {
			console.log(err);
			return next({name: 'Login'})
		}
	} else {
		next();
	}
})

export default router;
