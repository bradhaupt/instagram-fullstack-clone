//User Vuex Module
import router from "../../router/index.js"
import cookies from "vue-cookies";


const state = {
	user: {
		id: "",
		email: "",
		first_name: "",
		last_name: "",
		username: "",
		password: "",
		image: "",
	},
	xsrfToken: "",
	errorMessage: "",
	isLoading: false,
	controller: null,
	editUser: {
		username: "",
		email: "",
		first_name: "",
		last_name: "",
		password: "",
		image: "",
	},
	userProfile: null,
}

const mutations = {
	setCurrentUser: (state, auth) => {
		state.user = auth.user;
		state.xsrfToken = auth.xsrfToken !== undefined ? auth.xsrfToken : state.xsrfToken;
	},
	setErrorMessage: (state, message) => state.errorMessage = message,
	setIsLoading: (state, isLoading) => state.isLoading = isLoading,
	setController: (state, controller) => state.controller = controller,
	setEditUser: (state, user) => state.editUser = user,
	addFollower: (state, follower) => {
		state.userProfile.followers.push(follower);
		state.user.following.push(follower);
	},
	deleteFollower: (state, userId) => {
		state.userProfile.followers = state.userProfile.followers.filter(follower => follower.follower_id !== state.user.id);
		state.user.following = state.user.following.filter(following => following.user_id !== userId);
	},
	setUserProfile: (state, user) => state.userProfile = user
}

const actions = {
	async login({commit}, form) {
		try {
			// Read the CSRF token
			let xsrfToken = cookies.get("XSRF-TOKEN");

			// Then send login credentials
			const res = await fetch("/api/login", {
				method: "POST",
				headers: {
					'Content-Type': "application/json",
					Accept: 'application/json',
					'X-XSRF-TOKEN': xsrfToken
				},
				body: JSON.stringify(form)
			})

			if (!res.ok) {
				throw Error("No such user found");
			}
			xsrfToken = cookies.get("XSRF-TOKEN");

			const json = await res.json();
			commit('setCurrentUser', {user: json, xsrfToken});
			commit('setErrorMessage', "")
			router.push('/');
		} catch (err) {
			console.log(err.message);
			commit('setErrorMessage', err.message);
		}
	},
	async logout({commit, state}) {
		try {
			const res = await fetch("/api/logout", {
				method: "POST",
				headers: {
					'Content-Type': "application/json",
					Accept: 'application/json',
					'X-XSRF-TOKEN': state.xsrfToken
				},
			})

			if (!res.ok) {
				throw Error("Error while logging out");
			}

			commit('setCurrentUser', {user: null, xsrfToken: null});
			router.push("/login");
		} catch (err) {
			console.log(err);
		}
	},
	async createUser({}, user) {
		const xsrfToken = cookies.get("XSRF-TOKEN");

		const myHeaders = new Headers();
		myHeaders.append("Accept", "application/json");
		myHeaders.append("X-XSRF-TOKEN", xsrfToken);

		const formData = new FormData();
		formData.append('username', user.username);
		formData.append('email', user.email);
		formData.append('first_name', user.first_name);
		formData.append('last_name', user.last_name);
		formData.append('password', user.password);
		formData.append('password_confirmation', user.password_confirmation);

		if (user.fileInput) {
			formData.append("image", user.fileInput, ".jpg");
		}

		const requestOptions = {
			method: "POST",
			headers: myHeaders,
			body: formData,
		};

		try {
			const res = await fetch(`/api/users/`, requestOptions);

			if (!res.ok) {
				const errors = await res.json();
				if (errors?.errors) {
					return errors.errors;
				}
				throw errors;
			}

			router.push('/login');

		} catch (err) {
			console.log(err);
		}
	},
	async updateUser({commit, state}) {
		const myHeaders = new Headers();
		myHeaders.append("Accept", "application/json");
		myHeaders.append("X-XSRF-TOKEN", state.xsrfToken);

		const formData = new FormData();
		formData.append('username', state.editUser.username);
		formData.append('email', state.editUser.email);
		formData.append('first_name', state.editUser.first_name);
		formData.append('last_name', state.editUser.last_name);

		if (state.editUser.fileInput) {
			formData.append("image", state.editUser.fileInput, ".jpg");
		}

		formData.append("_method", 'PATCH');

		const requestOptions = {
			method: "POST",
			headers: myHeaders,
			body: formData,
		};

		try {
			const res = await fetch(`/api/users/${state.user.username}`, requestOptions);

			if (!res.ok) {
				throw Error("Couldn't update user");
			}

			const json = await res.json();
			commit('setCurrentUser', {user: json[0]});

		} catch (err) {
			console.log("error", err);
		}
	},
	async createFollower({commit, state, rootState}, user_id) {
		let myHeaders = new Headers();
		myHeaders.append("Accept", "application/json");
		myHeaders.append("Content-Type", "application/json");
		myHeaders.append("X-XSRF-TOKEN", rootState.user.xsrfToken);

		let requestOptions = {
			method: 'POST',
			headers: myHeaders,
			body: JSON.stringify({user_id}),
			redirect: 'follow'
		};
		try {
			const res = await fetch(`/api/follow/`, requestOptions)
			const json = await res.json()
			commit('addFollower', json);
		} catch (error) {
			console.log({error})
		}
	},
	async deleteFollower({commit, state, rootState}, user_id) {
		let myHeaders = new Headers();
		myHeaders.append("Accept", "application/json");
		myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
		myHeaders.append("X-XSRF-TOKEN", rootState.user.xsrfToken);

		let requestOptions = {
			method: 'DELETE',
			headers: myHeaders,
			redirect: 'follow'
		};
		try {
			const res = await fetch(`/api/follow/${user_id}`, requestOptions)
			const json = await res.json()

			if (json) {
				commit('deleteFollower', user_id)
			}
		} catch (error) {
			console.log({error})
		}
	},
	async fetchUserProfile({commit, dispatch, state}) {
		const username = router.currentRoute.params.username;
		commit('setUserProfile', null)
		commit('clearPosts');
		commit('setIsLoading', true)

		if (username === state.user.username) {
			commit('setUserProfile', state.user);
		} else {
			const controller = new AbortController();
			commit('setController', controller)
			const {signal} = controller;

			try {
				const res = await fetch(`/api/users/${username}`, {
					signal,
					headers: {
						Accept: 'application/json'
					}
				});

				if (!res.ok) {
					throw Error(`${res.status} ${res.statusText}`)
				}
				const json = await res.json();
				commit('setUserProfile', json)

			} catch (error) {
				commit('setIsLoading', false)
				if (error.name !== "AbortError") {
					commit('setErrorMessage', error.message)
					return console.log("Something went wrong", error);
				}
			}
		}
		await dispatch('fetchUserPosts', username)

	},
	setCurrentUser({commit}, {user, xsrfToken}) {
		commit('setCurrentUser', {user, xsrfToken});
	},
	setEditUser({commit}, user) {
		commit('setEditUser', user);
	},
}

const getters = {
	getCurrentUser: state => state.user,
	getToken: state => state.xsrfToken,
	getErrorMessage: state => state.errorMessage,
	getIsLoading: state => state.isLoading,
	getController: state => state.controller,
	getEditUser: state => state.editUser,
	getUserProfile: state => state.userProfile
}


export default {
	state,
	getters,
	actions,
	mutations
}
