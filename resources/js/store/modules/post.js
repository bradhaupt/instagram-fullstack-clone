import router from "../../router";

const state = {
	posts: [],
	nextPage: '/api/posts',
	editPost: {description: "", image: null},
	activePost: null,
	prevPost: null,
	nextPost: null,
	itemToDelete: {
		type: "",
		postSlug: "",
		commentId: null
	}
}

const mutations = {
	clearPosts(state) {
		state.posts = [];
	},
	setNextPage(state, page) {
		state.nextPage = page;
	},
	setPosts(state, posts) {
		state.posts = state.posts.concat(posts);
	},
	addPost(state, post) {
		state.posts.unshift(post);
	},
	updatePost(state, newPost) {
		state.posts = state.posts.map(post => post.slug === newPost.slug ? newPost : post);
	},
	deletePost(state) {
		state.posts = state.posts.filter(post => post.slug !== state.itemToDelete.postSlug);
		state.itemToDelete.postSlug = "";
	},
	setItemToDelete(state, item) {
		state.itemToDelete = item;
	},
	setEditPost(state, post) {
		state.editPost = post;
	},
	setActivePost(state, post) {
		state.activePost = post;
		const index = state.posts.findIndex(item => item.id === post.id);

		if(index === 0){
			console.log(index);
			if(state.posts.length === 1){
				state.nextPost = null;
				state.prevPost = null;
			} else{
			state.nextPost = state.posts[index+1];
			state.prevPost = null;
			}
		}else if(index === state.posts.length-1){
			state.prevPost = state.posts[index-1];
			state.nextPost = null;
		}else {
			state.nextPost = state.posts[index+1];
			state.prevPost = state.posts[index-1];
		}
	},
	addComment(state, comment) {
		const post = state.posts.find(post => post.id === comment.post_id);
		post.comments.unshift(comment);
	},
	deleteComment(state) {
		const post = state.posts.find(post => post.slug === state.itemToDelete.postSlug);
		post.comments = post.comments.filter(comment => comment.id !== state.itemToDelete.commentId);
		state.itemToDelete.commentId = null;
		state.itemToDelete.postSlug = "";
	},
	addLike(state, like) {
		const post = state.posts.find(post => like.post_id === post.id);
		post.likes.push(like);
	},
	deleteLike(state, like) {
		const post = state.posts.find(post => like.post_id === post.id);
		post.likes = post.likes.filter(item => item.id !== like.id)
	},
}

const actions = {
	async fetchNextPosts({commit, state}) {
		try {
			commit('setIsLoading', true);
			const controller = new AbortController();
			commit('setController', controller)
			const {signal} = controller;

			const res = await fetch(state.nextPage, {
				signal
			});
			const json = await res.json();

			commit('setPosts', json.data);
			commit('setNextPage', json.next_page_url)
			commit('setErrorMessage', "")
		} catch (error) {
			if (error.name !== "AbortError") {
				commit('setErrorMessage', error.message)
				console.log("Something went wrong", error);
			}
		} finally {
			commit('setIsLoading', false);
		}
	},
	async fetchUserPosts({commit}, username) {
		try {
			commit('setIsLoading', true);
			const controller = new AbortController();
			commit('setController', controller)
			const {signal} = controller;

			const res = await fetch(`/api/users/${username}/posts`, {
				signal
			});
			const json = await res.json();

			commit('setPosts', json.data);
			commit('setNextPage', json.next_page_url)
			commit('setErrorMessage', "")

		} catch (error) {
			if (error.name !== "AbortError") {
				commit('setErrorMessage', error.message)
				console.log("Something went wrong with the feed", error);
			}
		} finally {
			commit('setIsLoading', false);
		}
	},
	async createPost({commit, state, rootState}, post) {
		const myHeaders = new Headers();
		myHeaders.append("Accept", "application/json");
		myHeaders.append("X-XSRF-TOKEN", rootState.user.xsrfToken);

		const formData = new FormData();
		formData.append("image", post.fileInput, ".jpg");
		formData.append("description", post.description);

		const requestOptions = {
			method: 'POST',
			headers: myHeaders,
			body: formData,
			redirect: 'follow'
		}

		try {
			const res = await fetch("/api/posts/", requestOptions)

			if (!res.ok) {
				throw Error("Couldn't create post");
			}
			const json = await res.json();
			commit('addPost', json[0]);

			router.push('/');
		} catch (error) {
			console.log("Something went wrong", error);
		}
	},
	async updatePost({commit, state, rootState}) {
		const myHeaders = new Headers();
		myHeaders.append("Accept", "application/json");
		myHeaders.append("X-XSRF-TOKEN", rootState.user.xsrfToken);

		const formData = new FormData();
		formData.append('description', state.editPost.description);

		if (state.editPost.fileInput) {
			formData.append("image", state.editPost.fileInput, ".jpg");
		}
		formData.append("_method", 'PATCH');

		const requestOptions = {
			method: "POST",
			headers: myHeaders,
			body: formData,
		};
		try {
			const res = await fetch(`/api/posts/${state.editPost.slug}`, requestOptions);

			if (!res.ok) {
				throw Error("Couldn't update post");
			}
			const json = await res.json();
			commit('updatePost', json[0]);

		} catch (err) {
			console.log("error", err);
		}
	},
	async deletePost({commit, state, rootState}) {
		try {
			const res = await fetch(`/api/posts/${state.itemToDelete.postSlug}`, {
				method: "DELETE",
				credentials: 'include',
				headers: {Accept: "application/json", 'X-XSRF-TOKEN': rootState.user.xsrfToken}
			});

			if (!res.ok) {
				throw Error("Couldn't delete post");
			}
			const json = await res.json();

			if (json) {
				commit('deletePost');
			}
		} catch (err) {
			console.log(err);
		}
	},
	async createComment({commit, rootState}, comment) {
		const myHeaders = new Headers();
		myHeaders.append("Accept", "application/json");
		myHeaders.append("X-XSRF-TOKEN", rootState.user.xsrfToken);
		myHeaders.append("Content-Type", "application/json")

		try {
			const res = await fetch("/api/comments", {
				method: "POST",
				headers: myHeaders,
				body: JSON.stringify(comment)
			});

			if (!res.ok) {
				throw Error("Couldn't create comment");
			}
			const json = await res.json();

			commit('addComment', json[0])
		} catch (err) {
			console.log(err);
		}
	},
	async deleteComment({commit, state, rootState}) {
		const myHeaders = new Headers();
		myHeaders.append("Accept", "application/json");
		myHeaders.append("X-XSRF-TOKEN", rootState.user.xsrfToken);

		try {
			const res = await fetch(`/api/comments/${state.itemToDelete.commentId}`, {
				method: "DELETE",
				headers: myHeaders
			});

			if (!res.ok) {
				throw Error("Couldn't delete comment");
			}

			const json = await res.json();

			if (json) {
				commit('deleteComment');
			}
		} catch (err) {
			console.log(err);
		}
	},
	async createLike({commit, state, rootState}, post) {
		let myHeaders = new Headers();
		myHeaders.append("Accept", "application/json");
		myHeaders.append("Content-Type", "application/json");
		myHeaders.append("X-XSRF-TOKEN", rootState.user.xsrfToken);

		let requestOptions = {
			method: 'POST',
			headers: myHeaders,
			body: JSON.stringify({post_id: post.id}),
			redirect: 'follow'
		};
		try {
			const res = await fetch(`/api/likes/`, requestOptions)
			const json = await res.json()
			commit('addLike', json);
		} catch (error) {
			console.log({error})
		}
	},
	async deleteLike({commit, state, rootState}, like) {
		let myHeaders = new Headers();
		myHeaders.append("Accept", "application/json");
		myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
		myHeaders.append("X-XSRF-TOKEN", rootState.user.xsrfToken);

		let requestOptions = {
			method: 'DELETE',
			headers: myHeaders,
			redirect: 'follow'
		};
		try {
			const res = await fetch(`/api/likes/${like.id}`, requestOptions)
			const json = await res.json()

			if (json) {
				commit('deleteLike', like)
			}
		} catch (error) {
			console.log({error})
		}
	},

	setItemToDelete({commit}, item) {
		commit('setItemToDelete', item);
	},
	setEditPost({commit}, post) {
		commit('setEditPost', post)
	},
	setActivePost({commit}, post) {
		commit('setActivePost', post)
	},
	setNextPage({commit}, initPath) {
		commit('setNextPage', initPath)
	},
	clearPosts({commit}) {
		commit('clearPosts');
	},
}

const getters = {
	getAllPosts(state) {
		return state.posts;
	},
	getEditPost(state) {
		return state.editPost;
	},
	getActivePost(state) {
		return state.activePost;
	},
	getItemToDelete(state) {
		return state.itemToDelete;
	},
	getNextPage(state) {
		return state.nextPage;
	},
	getPrevPost(state){
		return state.prevPost;
	},
	getNextPost(state){
		return state.nextPost;
	}
}


export default {
	state,
	mutations,
	actions,
	getters,
}
